import global from '../component/global';

const registerhotel = (namehotel, price, dataimg, hoteltype, phone,
  date, address, lat, lng, tiennghi, website, userid) => (
    fetch(global.server + 'apptrivago/registerhotel.php',
    {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        },
        body: JSON.stringify({ namehotel, price, dataimg, hoteltype, phone,
          date, address, lat, lng, tiennghi, website, userid })
    })
    .then(res => res.json())
);

module.exports = registerhotel;
