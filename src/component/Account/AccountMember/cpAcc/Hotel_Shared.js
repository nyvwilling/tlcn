﻿import React, {Component} from 'react';
import {   
    View, 
    Text, 
    StyleSheet, 
    TouchableWithoutFeedback, 
    TouchableOpacity, 
    FlatList, 
    Dimensions,
    Image, 
    ScrollView, 
    TextInput, 
    ToastAndroid, 
    ActivityIndicator
} from 'react-native';

const { width, height } = Dimensions.get('window'); 

import global from '../../../global';

export default class HotelShared extends Component{
    constructor(props){
        super(props);
        this.state = {
            mang: ['1', '2', '3'],
            value: '',
            height: 40,
            isOpen: false,
            isDisabled: false,
            swipeToClose: true,
            sliderValue: 0.3,
            page: 1,
            refresh: false,
            index: -1,
            loading: false
        };
    }
    refresh() {
        this.setState({ page: 1, mang: []}, function() {
            //this.loadDataRefresh();
        });
        
    }
    render(){
        return(
            <View style={styles.container}>
                <FlatList
                ListFooterComponent={(
                        <View style= {{ padding: 10 }}>
                            {
                                !this.state.loading ?
                                    (
                                        <TouchableOpacity
                                        >
                                            <Text style={{ color: '#4267b2' }}>Load thêm danh sách ...</Text>
                                        </TouchableOpacity>) :
                                    (
                                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                            <ActivityIndicator size={24} />
                                            <Text>   Loading ...</Text>
                                        </View>
                                    )
                            }

                        </View>
                    )}
                    refreshing={this.state.refresh}
                    onRefresh={() => { this.refresh() }}
                    data={this.state.mang}
                    renderItem={({ index } ) =>
                        <View style={styles.rowFlatlist}>
                            <TouchableWithoutFeedback
                            >
                             <View style={{ height: width / 3, flexDirection: 'row', backgroundColor: 'white', borderRadius: 5 }}>
                                        <Image source={{ uri:'https://imgec.trivago.com/partnerimages/18/02/180235620_x.jpeg' }} style={{ height: width / 3, width: width / 3, flex: 1 }}/>
                                        <View style={{ flex: 2 }}>
                                            <View style={{ flex: 1 }}>
                                                <View style={{ flex: 1, paddingLeft: 5, paddingVertical: 2, paddingRight: 2 }}>
                                                    <Text numberOfLines={1} style={{ fontWeight: 'bold' }}>Lakeview Villas And Viet Nam</Text>
                                                    <Text numberOfLines={1}>15 Tran Phu Street, 670000, Đà Lạt, Việt Nam}</Text>
                                                </View>
                                                <View style = {{ flex: 2, borderTopWidth: 1, borderTopColor: '#e9ebee', flexDirection: 'row' }}>
                                                    <View style={{ flex: 1 }}>
                                                       
                                                        <View style={{ flex: 1, paddingHorizontal: 2, paddingVertical: 2 }}>
                                                            <TouchableOpacity
                                                                style={{flexDirection: 'row', flex: 1 }}
                                                            >
                                                                <View style={{ flex: 2, paddingVertical: 2 }}>
                                                                    <Image resizeMode={'contain'} style={{ flex: 1 }} />
                                                                </View> 
                                                                
                                                                <View style={{ flex: 3, alignItems: 'center', justifyContent: 'center' }}>
                                                                    <Text style={{ fontSize: 12 }}>Mở khóa</Text>
                                                                </View>
                                                            </TouchableOpacity>
                                                        </View>
                                                       
                                                        <View style={{ flex: 1, borderTopWidth: 1, borderTopColor: '#e9ebee', paddingHorizontal: 2, paddingVertical: 2}}>
                                                        <TouchableOpacity
                                                                style={{flexDirection: 'row', flex: 1 }}
                                                        >
                                                                <View style={{ flex: 2, paddingVertical: 2 }}>
                                                                    <Image resizeMode={'contain'} style={{ flex: 1 }} />
                                                                </View> 
                                                                
                                                                <View style={{ flex: 3, alignItems: 'center', justifyContent: 'center' }}>
                                                                    <Text style={{ fontSize: 12 }}>Xóa</Text>
                                                                </View>
                                                        </TouchableOpacity>
                                                        
                                                        </View>
                                                    </View>
                                                    <View style={{ flex: 1, borderLeftWidth: 1, borderLeftColor: '#e9ebee'}}>
                                                        <View style={{ flex: 1, paddingHorizontal: 2, paddingVertical: 2 }}>
                                                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                                                <Text style={{ fontSize: 10 }}>Giá từ</Text>
                                                                <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#248f24' }}>300000</Text>
                                                            </View>
                                                        </View>

                                                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                                            <TouchableOpacity
                                                            >
                                                                <Text style = {{ backgroundColor: '#248f24', paddingHorizontal: 8, paddingVertical: 4, borderRadius: 3, color: 'white' }}>Chi tiết</Text>
                                                            </TouchableOpacity>
                                                        </View>

                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                            </TouchableWithoutFeedback>
                        </View>
                    }
                /> 
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    rowFlatlist: {
        paddingHorizontal: 5,
        paddingVertical: 2.5
        //borderBottomWidth: 1
    },
    itemBottom: {
        backgroundColor: 'white'
    }
})