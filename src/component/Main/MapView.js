import React, { Component } from 'react';
import {
    View, StyleSheet, Dimensions, Text
} from 'react-native';
import MapView from 'react-native-maps';
import PopupDialog, {
   SlideAnimation,
   DialogTitle,
   DialogButton
} from 'react-native-popup-dialog';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
const { height, width } = Dimensions.get('window');
export default class MapViewComponent extends Component {
    constructor(props){
      super(props);
      arraymarker=[{
        latitude: 10.863703521363849,
        longitude: 106.8026926741004
      }];
      this.state = {
        isModalVisible: false,
        mang: [
                  {
                      "latitude" : "10.8469059",
                      "longitude" : "106.7979073"
                  },
                  {
                      "latitude" : "10.868751477356128",
                      "longitude" : "106.7698483541608"
                  },
                  {
                      "latitude" : "10.831535327524618",
                      "longitude" : "106.77498679608107"
                  }
              ],
          coordinate: arraymarker
      };
    }
    componentDidMount(){
      for ( var i = 0, len = this.state.mang.length; i < len; i++) {
          let latitude = parseFloat(this.state.mang[i].latitude);
          let longitude = parseFloat(this.state.mang[i].longitude);
          arraymarker.push({
            latitude: latitude,
            longitude: longitude
          });
          this.setState({
            coordinate: arraymarker
          });
      }
    }
    onSelect(index) {
        this.popupDialog.dismiss(() => {
        });
    }
    showModal = () => this.setState({ isModalVisible: true });
    renderMarker(){
      var markers = [];
      for (marker of this.state.coordinate) {
         markers.push(
           <MapView.Marker
           key={marker.longitude}
           coordinate={marker}
          //  onPress={() => {this.props.navigate('DetailScreen', {name: 'AAAAAAAAA'}) }}
           onPress={() => { this.popupDialog.show() }}
           />
         );
      }
      return markers;
    }
    onPress(data){
      // let latitude = data.nativeEvent.coordinate.latitude;
      // let longitude = data.nativeEvent.coordinate.longitude;
      // arraymarker.push({
      //   latitude: latitude,
      //   longitude: longitude
      // });
      // this.setState({
      //   coordinate: arraymarker
      // });
      // console.log(latitude+"   "+longitude);
    }
    render(){
        return(
            <View style = {styles.container}>
              <MapView
                style={styles.map}
                region={{
                  latitude: 10.8469059,
                  longitude: 106.7979073,
                  latitudeDelta: 0.15,
                  longitudeDelta: 0.121,
                }}
                onPress={this.onPress.bind(this)}
              >
              {this.renderMarker()}
              </MapView>
              <PopupDialog
                  width={0.9 * width}
                  ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                  dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
                  dialogTitle={<DialogTitle title="Sort by" />}

              >
                  <View style={{ margin: 10 }}>
                      <RadioGroup
                          selectedIndex = {0}
                          onSelect = {(index) => {this.onSelect(index)}}
                      >
                          <RadioButton value={'item1'} >
                          <Text>Sort by Distance</Text>
                          </RadioButton>

                          <RadioButton value={'item2'}>
                          <Text>Sort by Popularity</Text>
                          </RadioButton>

                          <RadioButton value={'item3'}>
                              <Text>Focus on Rating</Text>
                          </RadioButton>
                          <RadioButton value={'item4'}>
                              <Text>Focus on Price</Text>
                          </RadioButton>
                      </RadioGroup>
                  </View>
              </PopupDialog>
            </View>
        )
    }
}
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: height,
    width: width,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  }
})
