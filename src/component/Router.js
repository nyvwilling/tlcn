﻿import React from 'react';
import {
    Dimensions
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import Main from './Main/Main';
import MainView from './Main/MainView';
import Menu from './Menu/Menu';
import SignIn from './Account/SignIn';
import SignUp from './Account/SignUp';
import AccountMember from './Account/AccountMember/Account';
import Search from './Search/Search';
import Details from './Details/Details';
import Welcome from './Welcome/Welcome';
import LikeHotels from './Account/AccountMember/cpAcc/LikeHotels';
import HotelShare from './Account/AccountMember/cpAcc/HotelShare';
import LocationAddress from './Account/AccountMember/cpAcc/Location_Address';
import ChangePass from './Account/AccountMember/cpAcc/ChangePass';
import UploadImg from './Account/AccountMember/cpAcc/UploadImg';
import HotelShared from './Account/AccountMember/cpAcc/Hotel_Shared';

var { height, width } = Dimensions.get('window');

export const MainStack = StackNavigator({
  WelcomeScreen:{
      screen: Welcome,
      navigationOptions: {
          header: null
      }
  },
    MainScreen: {
        screen: Main,
        navigationOptions: {
            header: null
        }
    },
    SigninScreen: {
        screen: SignIn,
        navigationOptions: {
            headerTitle: 'Quay lại'
        }
    },
    SignupScreen: {
        screen: SignUp,
        navigationOptions: {
            headerTitle: 'Quay lại đăng nhập'
        }
    },
    SearchScreen: {
        screen: Search
    },
    DetailScreen: {
        screen: Details
    },
    AccountScreen: {
        screen: AccountMember,
        navigationOptions: {
            headerTitle: 'Quay lại'
        }
    },
    LikeScreen: {
        screen: LikeHotels,
        navigationOptions: {
            headerTitle: 'Quay lại'
        }
    },
    ShareScreen: {
        screen: HotelShare,
        navigationOptions: {
            headerTitle: 'Quay lại'
        }
    },
    LocationScreen: {
        screen: LocationAddress,
        navigationOptions: {
            header: null
        }
    },
    ChangePassScreen: {
        screen: ChangePass,
        navigationOptions: {
            headerTitle: 'Quay lại'
        }
    },
    UploadImgScreen: {
        screen: UploadImg,
        navigationOptions: {
            headerTitle: 'Quay lại'
        }
    },
    HotelSharedScreen: {
        screen: HotelShared,
        navigationOptions: {
            headerTitle: 'Quay lại'
        }
    }
});

export const SideMenu = DrawerNavigator (
    {
        MainSide: {
            screen: MainStack
        }
    },
    {
        drawerWidth: 0.8 * width,
        drawerPosition: 'left',
        contentComponent: props => <Menu {...props} />
    }
)
// export const WelcomeStack = StackNavigator({
//     WelcomeScreen:{
//         screen: Welcome,
//         navigationOptions: {
//             header: null
//         }
//     },
//     MainScreen:{
//         screen: SideMenu,
//         navigationOptions: {
//             header: null
//         }
//     },
//     SigninScreen:{
//       screen: SignIn,
//       navigationOptions:{
//         headerTitle: 'Welcome'
//       }
//     },
//     SignupScreen: {
//         screen: SignUp,
//         navigationOptions: {
//             headerTitle: 'Đăng nhập'
//         }
//     },
//     AccountScreen: {
//         screen: Account,
//         navigationOptions: {
//             headerTitle: 'Welcome'
//         }
//     }
//
// });
